\begin{resumo-aula}{14}{3}{2012}
Provamos que a extensão de uma \mencao{medida de probabilidade} de uma álgebra para uma \sigal\ é possível, preservando os valores da medida nos pontos da álgebra. O passo seguinte é demonstrar que essa extensão é única.
\end{resumo-aula}

\begin{exemplo}
  \(\omg=(0,1]\), \(\borel_0\) intervalos que são uniões finitas de intervalos disjuntos, \(A\in \borel_0\), \(A\in \omg\) se \(A=\cup_{i=1}^nI_i=\cup_{i=1}^n(a_i,b_i]\).

Defina \(\prob{A}\deq\sum_{i=1}^n|I_i| = \sum_{i=1}^n(b_i-a_i)\), \(A\in\borel_0\). Iremos provar que \prob{A} é enumeravelmente aditiva, mais tarde.
\end{exemplo}

\begin{definicao}
Se \famf\ é ima \sigal\ em \omg, \P\ uma medida em \famf, então a trinca \((\omg,\famf,\P)\) é dita um \defi{espaço de probabilidade}.
\end{definicao}

\begin{definicao}
  Um \defi{suporte} de \P\ é qualquer conjunto \(A\), \famf-mensurável, tal que \(\prob{A}=1\).
\end{definicao}

\begin{observacao}
  Considere \P\ uma medida de probabilidade em uma álgebra \famf, e \(A\subset B\), então \(\prob{A}\leq\prob{B}\). (Monótona.)
\end{observacao}

\begin{prova}
  Ideia: vide \ref{fig:venn-A-inside-B}). Podemos decompor o conjunto \(B\) em \(B=(B\cap\compl{A})\cup A=(B\setminus A)\cup A\). Portanto, \(\prob{B}=\prob{B\setminus A} + \prob{A}\leq\prob{A}\). Além disso, \(\prob{B\setminus A}=\prob{B} - \prob{A}\). Também, tomando \(B=\omg\), segue que \(\prob{\compl{A}}=1-\prob{A}\).\qed
\end{prova}

\begin{figure}
\begin{center}
  \begin{tikzpicture}
    \fill [orange!60,rounded corners] (-3,-2) rectangle (3,2);
    \node [white,circle,minimum size=3cm,fill,label=30:{\(B\)}] {};
    \node [orange!40,circle,minimum size=1.5cm,fill,label=30:{\(A\)}] {};
  \end{tikzpicture}
\caption{Diagrama de Venn: \(A\subset B\).}\label{fig:venn-A-inside-B}
\end{center}
\end{figure}

\begin{teorema}
Se \P\ é medida de probabilidade em uma álgebra \famf, então
\begin{enumerate}[i)]
\item \(\anseq\in\famf\), \(A\in\famf\), e \(\an\uparrow A\) (ou seja, para todo \(n\geq1\) vale \(\an\subset A_{n+1}\)) então \(\prob{\lim_n\an}=\lim_n\prob{\an}\).\label{teo:limites-monotonos-i}
\item \anseq, \(A\in\famf\), e \(\an\downarrow A\) então \(\prob{\an}\downarrow\prob{A}\).
\item \anseq\ e \(\uniaoinfinita{k}{A_k}\in\famf\), então  \(\probcol{\displaystyle\uniaoinfinita{k}{A_k}}
\leq\displaystyle\sum_{k=1}^\infty\prob{A_k}\) (enumeravelmente sub-aditivo).
\end{enumerate}
\end{teorema}

\begin{prova}
\begin{enumerate}[i)]
\item Temos que mostrar que \(\prob{\an}\uparrow\prob{A}\). Sejam \(B_1=A_1,B_k=A_k\cap \compl{A_{k-1}}\).
\begin{center}
\begin{tikzpicture}
  \fill [orange!60,rounded corners] (-3,-2) rectangle (3,2);
  \node [white,circle,minimum size=3.5cm,fill,label=45:{\(\udots\)}] {};
  \node [orange!60,circle,minimum size=2cm,fill,label=45:{\(A_2\)}] {};
  \node [white,circle,minimum size=.5cm,fill,label=45:{\(A_1\)}] {};
\end{tikzpicture}
\end{center}

\(B_k\) são disjuntos, e \(A=\bigcup_{n=1}^\infty\an=\bigcup_{n=1}^\infty B_k\). Além disso, \({\an=\bigcup_{k=1}^nB_k}\). Segue que \(\prob{\uniaoinfinita{n}{\an}}=\prob A=\prob{\uniaoinfinita{k}{B_k}}=\somainfinita{n}{\prob{B_k}}\deq\lim_{n\to\infty}\somafinita{k}{n}{\prob{B_k}}=\lim_{n\to\infty}\prob{\uniaofinita{k}{n}{B_k}}=\lim_{n\to\infty}\prob{\an}\)
\item \(\an\downarrow A\), então \(\compl{\an}\uparrow \compl{A}\) e portanto \(1 -\prob{\an}\uparrow 1 -\prob{A}\).
\item Seja \(B_1=A_1\) e \(B_k=A_k\bigcap \compl{A_{k-1}}\bigcap \cdots\bigcap\compl{A_1}\). Os \(\{B_k:k\geq 1\}\) são disjuntos. Além disso, \(\uniaofinita{k}{n}{A_k} = \uniaofinita{k}{n}{B_k}\). Segue que  \(\prob{\uniaofinita{k}{n}{A_k}} = \somafinita{k}{n}{\prob{B_k}}\). Como \(\prob{B_k}\leq\prob{A_k}\), temos que \(\prob{\uniaofinita kn{A_k}}\leq\somafinita kn{\prob{A_k}}\) (desigualdade de Boole). Segue que \(\prob{\uniaofinita kn{A_k}}\leq \somainfinita k{\prob{A_k}}\). Aplicando a parte \ref{teo:limites-monotonos-i} do teorema, o resultado segue.\qed
\end{enumerate}
\end{prova}
Considere \P\ uma medida de probabilidade em uma álgebra \(\famf_0\) de subconjuntos de \omg\ e defina \(\famf=\sigma(\famf_0)\). Vamos mostrar que existe uma medida de probabilidade \(\mathcal{Q}\) em \famf\ tal que \(\mathcal{Q}(A)\) é igual a \prob{A}, para todo \(A\in\famf_0\).

Iremos mostrar também que se \(\mathcal{Q}'\) é outra medida de probabilidade em \famf, tal que \(\mathcal{Q}'(A)=\prob{A}, A\in\famf\), então \(\mathcal{Q}'(A)=\mathcal{Q}(A)\) para \(A\in\famf_0\).

\begin{definicao}
Para \(A\in\omg\), definimos sua \defi{medida exterior} por
\[
\medidaexterior{A}\deq\inf\sum_n\prob{\an}
\]
onde o ínfimo é tomado sobre todas as sequências, finitas ou infinitas, \(A_1,A_2,\ldots\) de conjuntos \(\famf_0\)-mensuráveis satisfazendo \(A\subset \bigcup_n\an\). (O ínfimo está bem definido porque sempre existe ínfimo de uma sequência de números reais; e o somatório está definido porque os \an\ estão em uma álgebra.)
\end{definicao}

\begin{definicao}
  Para \(A\in\omg\), definimos a \defi{medida interior} por
\[
\medidainterior{A}\deq 1-\medidaexterior{\compl{A}}.
\]
\end{definicao}

Note que as medidas são iguais quando \(\medidaexterior{A} + \medidaexterior{\compl{A}} = 1\).

\begin{definicao}
Um conjunto \(A\in\omg\) é dito \Pexterior-mensurável se \[\medidaexterior{A\cap E} + \medidaexterior{\compl{A}\cap E} = \medidaexterior{E},\] para todo \(E\in\omg\).
\end{definicao}

Seja \famm\ a classe definida por
\begin{eqnarray*}
\famm&\deq& \bigl\{A:\medidaexterior{A\cap E} + \medidaexterior{\compl{A}\cap E} = \medidaexterior{E},\forall E\in\omg\bigr\}\\
&\deq& \{A : A\text{ é \Pexterior-mensurável}\}
\end{eqnarray*}

\verif
\begin{itemize}[1)]
\item \(\medidaexterior{\vzio}=0\),
\item \(\medidaexterior{A}\geq 0\) para todo \(A\in\omg\),
\item \(A\subset B\implies\medidaexterior{A}\leq\medidaexterior{B}\).
\end{itemize}

\begin{lema}
\(\displaystyle\medidaexterior{\bigcup_n\an}\leq\sum_n\medidaexterior{\an}\) (enumeravelmente subaditiva).
\end{lema}

\begin{prova}
Para \(\varepsilon>0\), escolha conjuntos \(B_{nk}\) que sejam \(\famf_0\)-mensuráveis tais que \(\an\subset\bigcup_k B_{nk}\) e \(\sum_k \prob{B_{nk}}<\medidaexterior{\an}+\varepsilon/2^n\), o que é possível pela definição de ínfimo. Temos que \(\bigcup_n\an\subset\bigcup_{n,k} B_{nk}\) tal que \(\medidaexterior{\bigcup_n \an}\leq \sum_{n,k}\prob{B_{nk}}<\sum_n \medidaexterior{\an}+\varepsilon\).

Portanto, como \(\varepsilon>0\) é arbitrário,
\[
\medidaexterior{\bigcup_n \an}\leq\sum_n\medidaexterior{\an}.\xqed
\]
\end{prova}

\begin{lema}
\famm\ é uma álgebra.
\end{lema}

