\begin{resumo-aula}{21}{3}{2012}
Limites de conjuntos. Exibimos o exemplo de um conjunto não-Boreliano que está nas partes de \((0,1]\).
\end{resumo-aula}

\begin{definicao}
\anseq\ eventos (ou seja, conjunto mensuráveis). Sejam
\begin{eqnarray*}
\limsup_n\an&\deq&\bigcap_{n=1}^\infty\bigcup_{k=1}^\infty A_k, \text{\quad e }\\
\liminf_n\an&\deq&\bigcup_{n=1}^\infty\bigcap_{k=1}^\infty A_k.
\end{eqnarray*}
\end{definicao}

\begin{definicao}
  Se \(\liminf_n\an = \limsup_n\an=A\) então escrevemos \(\lim_n\an\deq\liminf_n\an\limsup_n\an\), isto é, \(\an \to_{n\to \infty} A\).
\end{definicao}

\begin{exemplo}
Seja \(A\subset B\), propriamente, e defina \(A_{2n}\deq A\) e \(A_{2n+1}\deq B\), para \(n\geq 1\). Então \(A=\liminf_n\an\neq\limsup_n\an=B\).
\end{exemplo}

\begin{teorema}
\[\prob{\liminf_n\an}\leq\liminf_n\prob{\an}\leq\limsup_n\prob{\an}\leq\prob{\limsup_n\an}\](Obs:Uma maneira de descrever \(\liminf\) de uma sequência de números é como o maior ponto de acumulação abaixo da sequência.)
\end{teorema}

\begin{prova}
Sejam \(B_n\deq\bigcap_{k=n}^\infty A_k\) e \(C_n\deq\bigcup_{k=n}^\infty A_k\). Então \(B_n\uparrow\uniaoinfinita{n}{B_n}=\liminf_n\an\) e \(C_n\downarrow\intersecaoinfinita{n}{C_n}=\limsup_n\an\). Pela continuidade (por cima e por baixo) já provada, temos que
\begin{eqnarray*}
\prob{\an}&\geq&\prob{B_n}\rightarrow\prob{\liminf_n\an}\text{ e}\\
\prob{\an}&\leq&\prob{C_n}\rightarrow\prob{\limsup_n\an}.
\end{eqnarray*}
Portanto 
\begin{eqnarray*}
\prob{\liminf_n\an}&\leq&\liminf_n\prob{\an} \quad\left(\;\parbox{4cm}{ pois \(\liminf\) e \(\limsup\) de uma sequência sempre existem.}\;\right)\\
&\leq&\limsup_n\prob{\an}\leq\prob{\limsup_n\an}.\xqed
\end{eqnarray*}
\end{prova}

Veremos agora um exemplo de conjunto em \((0,1]\) que não 
e Borel-mensurável. Faremos algumas observações para embasar o exemplo.

O que fizemos até agora: estendemos a medida para uma \sigal, e mostramos que a extensão é única. Exibimos monotoicidade e continuidade da medida \P\ na trinca \((\omg,\famf,\P)\). Justificamos agora nosso trabalho, mostrano que de fato \emph{existe gente} que não podemos pegar com esse modelo.

Antes, porém, comentamos o \mencao{Axioma da Escolha}.

\begin{observacao}
\mencao{Axioma da Escolha}: Suponha \(\{A_\theta:\theta\in\Theta\}\) uma decomposição de~\omg\ --- isto é, uma partição não-enumerável de~\omg; em outras palavras \(\bigcup_\theta A_\theta=\omg\) e vale \(A_\theta\cap A_{\theta'}=\vzio\) se \(\theta\neq\theta'\). O~\defi{Axioma da Escolha} estabelece que \emph{existe} ao menos um conjunto \(C\) que contém exatamente um ponto de cada \(A_\theta\), isto é, \(A\theta\cap C\) é um conjunto unitário para todo \(\theta\in \Theta\), com \(A_\theta\neq\vzio, \forall\theta\in\Theta\).
\end{observacao}

\begin{observacao}
Uma \defi{relação} definida em um conjunto \omg\ é uma função de \(\omg\times\omg\) que vale \(1\) ou \(0\) (função booleana). Isto é, dados \(x,y\in\omg\), ou \(x\) se relaciona com \(y\) (indicamos \(x\sim y\)), ou \(x\) não se relaciona com \(y\) (\(x\nsim y\)). Uma relação é de \defi{equivalência} se é
\begin{enumerate}[a)]
\item \defi{reflexiva}: \(x\sim x\) para todo \(x\in\omg\);
\item \defi{simétrica}: \(x\sim y\implies y\sim x\) para todo \(x,y\in\omg\); e
\item \defi{transitiva}: \(x\sim y \land y\sim z\implies x\sim z\), para todo \(x,y,z\in\omg\).
\end{enumerate}

Dada uma relação de equivalência \(\sim\), e \(x\in\omg\), a \defi{classe de equivalência} de \(x\) são todos \(y\in\omg\) tais que \(x\sim y\). As classes de equivalência formam uma \mencao{decomposição} de \omg. Observe que duas classes (em uma relação de equivalência) ou são idênticas ou são disjuntas.
\end{observacao}

Passamos agora à construção do conjunto não-Boreliano, devida a Vitali.
\begin{exemplo}
Definimos a adição módulo \(1\) em \((0,1]\): para \(x,y\in(0,1]\), 
\[
x\oplus y\deq
\begin{cases}
x+y & \text{se \(x+y \leq 1\)}\\
x+y -1 & \text{caso contrário}
\end{cases}
\]

Definimos ainda \(A\oplus x\deq [a\oplus x: a\in A]\), onde usamos colchetes porque não necessariamente tratamos de um conjunto.

Defina a classe \faml\ de subconjuntos de \(\omg=(0,1]\) por:
\[
\faml\deq \{A:A\in\borel,\quad A\oplus x\in \borel \quad \text{ e }\quad \lambda(A\oplus x) = \lambda(A)\}
\]
onde \borel\ são os borelianos do intervalo \((0,1]\), e \(\lambda\) é o comprimento (\defi{medida de Lebesgue}: \(\lambda\bigl( (a,b] \bigr)\deq b -a\)).

Veja que \faml\ é um \lambdasistema\ contendo os intervalos, e portanto \(\borel\subset\faml\) (usando o lema de Dynkin). Segue que \(A\in\borel\implies A\oplus x\in\borel\) e \(\lambda(A\oplus x)=\lambda(A)\).

Definimos \(x\) e \(y\) equivalentes (\(x\sim y\)) se \(x\oplus r=y\), para algum \(r\) racional. Considere \(H\) o subconjunto de \((0,1]\) formado por um representante de cada classe de equivalência, e considere a classe (enumerável) de conjuntos \(H\oplus r\), com \(r\) racional em \((0,1]\). Estes conjuntos são disjuntos e cada ponto de \((0,1]\) cai em algum desses conjuntos. Segue que \((0,1]=\bigcup_r (H\oplus r)\), uma união enumerável de conjuntos disjuntos. 

Se \(H\in\borel\), então \(\lambda\bigl( (0,1] \bigr)=\sum_r \lambda (H\oplus r)\). Isto é impossível (porque o lado esquerdo é 1 e o direito é zero ou \(+\infty\)).
\end{exemplo}
