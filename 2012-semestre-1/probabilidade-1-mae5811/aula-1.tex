\begin{resumo-aula}{9}{3}{2012}
 Definição de álgebra, \sigal, espaço de Borel, medida de probabilidade e espaço de probabilidade.
\end{resumo-aula}

Considere \omg\ um conjunto arbitrário não-vazio (\omg\ será dito o \mencao{espaço amostral} do experimento aleatório), e \famf\ uma classe de subconjuntos de \omg.

\famf\ é dita uma \defi{álgebra} de subconjuntos de \omg\ se $\omg \in \famf$, se é fechada por complementação e por uniões finitas de conjuntos de \famf. Isto é \famf\ é uma álgebra se
\begin{enumerate}[1)]
\item $\omg\in\famf$,\label{def:algebra-i}
\item $A\in\famf\implies \compl{A}\in\famf$,\label{def:algebra-ii}
\item $A,B\in\famf\implies A\cup B \in\famf$.\label{def:algebra-iii}
\end{enumerate}

\begin{observacao}
  Se vale \ref{def:algebra-i}, e \ref{def:algebra-ii}, temos que \ref{def:algebra-iii} é equivalente a ($=$~suficiente e necessário) $A,B \in \famf\implies A\cap B\in\famf$.
\end{observacao}

Para verificar, lembre das operações de De Morgan\footnote{Augustus De Morgan, inglês nascido em Madurai, na Índia, em 1806.}:
\[
\compl{(A\cup B)} = \compl{A} \cap \compl{B} 
\qquad \text{ e } \qquad 
\compl{(A\cap B)} = \compl{A} \cup \compl{B} 
\]

\famf, classe de subconjuntos de \omg, é dita uma \defi{\sigal} se ela for uma álgebra fechada por uniões enumeráveis de subconjuntos de \famf. Isto é, \famf\ é \sigal\ de \omg\ se:
\begin{enumerate}[1)]
\item $\omg\in\famf$, \label{def:sigal-i}
\item $A\in\famf\implies\compl{A}\in\famf$, e\label{def:sigal-ii}
\item $A_1,A_2,\ldots\in\famf\implies\bigcup_{n=1}^\infty  A_i\in\famf$. \label{def:sigal-iii}
\end{enumerate}

\begin{observacao}
  Um conjunto $A\in\famf$ é dito \famf-mensurável.
\end{observacao}

\begin{observacao}
Definimos \partes{\omg} (``partes de \omg'') como o conjunto de todos os subconjuntos de \omg\ (também denotado \twoto\omg). As \partes{\omg} sempre são \sigals. Além disso,
$\famf_0\deq\{\vzio,\omg\}$ é a \sigal\ \defi{trivial}, com cardinalidade $|\famf_0|=2$, e se $A\subset\famf$, então $\famf_A\deq\nobreak\{\vzio,A,\compl A,\omg\}$ é dito \defi{traço} de $A$. Temos $\famf_0\subset\famf_A\subset\partes \omg$.
\end{observacao}

\begin{exemplo}
\(\omg=(0,1]\), e \(0<a\leq b\leq 1\); e seja $|I|=\bigl|(a,b]\bigr|=b-a$. Considere conjuntos $A=\bigcup_{i=1}^n (a_i,b_i]=\bigcup_{i=1}^nI_i$ onde os intervalos $I_i$ são disjuntos e contidos em $\omg=(0,1]$.

Essa é uma classe fechada por uniões finitas de conjuntos disjuntos de $(0,1]$. Incluindo o conjunto vazio \vzio, esta classe, digamos $\borel_0$, é uma \mencao{álgebra}.
\end{exemplo}

\begin{prova}
Mostramos que \(\borel_0\) satisfaz \ref{def:sigal-i}): \(\omg\in\borel_0\).  Para \ref{def:sigal-ii}), seja \(A\deq (a_1,a_1']\cup(a_2,a_2']\cup\cdots\cup(a_n,a_n']\) com \(a_1\leq a_2\leq\cdots\leq\nolinebreak a_n\). Se os \((a_i,a_i']\) são disjuntos, então \(\compl{A}=(0,a_1]\cup(a_1',a_2]\cup\cdots\allowbreak\cdots\cup(a_{n-1}',a_n]\in\borel_0\) (alguns destes intervalos podem ser vazios). Finalmente, se \(B\deq(b_1,b_1']\cup(b_2,b_2']\cup\cdots\cup(b_m,b_m']\) disjuntos então \[A\cap B=\bigcup_{i=1}^n\bigcup_{j=1}^m\biggl\{\bigl(a_i,1_i'\bigr]\cap\bigl(b_i,b_i'\bigr]\biggr\}\] onde cada parcela é um intervalo disjunto ou vazio, e a união é uma união finita. Sendo assim, \(\borel_0\) é uma álgebra de subconjuntos de \((0,1]\).\qed
\end{prova}

E \(\borel_0\) não é uma \sigal.
\begin{prova}
\[
\bigcap_{n=1}^\infty\left(\!x-\frac{1}{n},x\right]=\{x\}\notin\borel_0.\xqed
\]
\end{prova}

\begin{definicao}
Para uma classe \fama\ de subconjuntos de \omg, definimos \(\sigma(\fama)\) como a menor (em termos de ``estar contido'') \sigal\ que contém \fama. É dita a \sigal\ gerada por \fama. Em outras palavras, \(\sigma(\fama)\) é por definição a interseção de todas as \sigals\ que contém \fama.
\end{definicao}

\begin{nota}
  A união de \sigals\ não é, em geral, uma \sigal.
\end{nota}

\begin{observacao}
  \begin{enumerate}[1)]
  \item \(\fama\subset\sigma(\fama)\),
  \item \(\sigma(\fama)\) é uma \sigal,
  \item se \(\fama\subset\fami G \), e \fami G é \sigal, então \(\sigma(\fama)\subset\fami G\),
  \item se \famf\ é \sigal, então \(\sigma(\famf)=\famf\),
  \item se \(\fama\subset\fami A'\), então \(\sigma(\fama)\subset\sigma(\fami A')\),
  \item \(\fama\subset\fami A' \subset \sigma(\fama)\) implica \(\sigma(\fami A') = \sigma(\fama)\).
  \end{enumerate}
\verif
\end{observacao}

\begin{exemplo}
Considere \(\omg=(0,1]\) e defina \fama\ como a classe formada por intervalos de \omg. Seja \(\borel\deq\sigma(\fama)\). Observe que \(\fama\subset\borel_0\subset\sigma(\fama)\implies\borel=\sigma(\borel_0)\).

\borel\ é dita a \defi{\sigal\ de Borel}, e \(B\in\borel\) é um \defi{Boreliano}. Veremos que \borel\ \emph{não~contém} todos os subconjuntos de \omg!!!\footnote{A cardinalidade de \borel\ é maior que a da reta --- supera \(\aleph_0\).}\fimexemplo
\end{exemplo}

\begin{definicao}
  Uma \defi{função de conjuntos} é uma função real (com valor em \reais) definida em alguma classe de subconjuntos de \omg.
\end{definicao}

\begin{definicao}
  Uma função de conjuntos \P\ definida em uma álgebra \famf\ é uma \defi{medida de~probabilidade} se
\begin{enumerate}[1)]
\item \(0\leq\prob{A}\leq1\), para \(A\in\famf\),
\label{def:medida-prob-i}
\item \(\prob{\vzio}=0\), e \label{def:medida-prob-ii}
\item se \(A_1,A_2,\ldots\) são conjuntos disjuntos \famf-mensuráveis, e se vale \(\bigcup_{i=1}^\infty A_i\in\famf\) então \(\prob{\bigcup_{i=1}^\infty A_i}=\sum_{i=1}^\infty \prob{A_i}\).\label{def:medida-prob-iii}
\end{enumerate}
\end{definicao}

\begin{observacao}
A condição \ref{def:medida-prob-iii}) diz que \P\ é \defi{enumeravelmente aditivo}.
\end{observacao}

\begin{observacao}
  Se \(A_1,A_2,\ldots,A_n\) são \famf-conjuntos disjuntos, então \(\bigcup_{i=1}^n A_i\in\famf\). Colocando \(A_{n+1} = A_{n+2} = \ldots = \vzio\), temos que \(\prob{\bigcup_{i=1}^\infty A_i} = \prob{\bigcup_{i=1}^n}\), ou seja, é \defi{finitamente aditiva}.
\end{observacao}

\[
\borel\subset\lebesgue\subset\partes{\omg}
\]
