\begin{resumo-aula}{14}{08}{2012}
  Desigualdade de \Janson\ (sobre desvios ``para baixo''), cota de \Janson-\Luczak-\Rucinski.
\end{resumo-aula}

Como antes, temos o conjunto aleatório \(\Gamma_{p_1,\ldots,p_n}\subset\Gamma=[n]=\{1,\ldots,n\}\), as probabilidades \(0\leq p_1,\cdots, p_n\leq 1\) (de \(i\in\Gamma_{p_1,\ldots,p_n}\) --- independentes) e denotamos \(p=(p_1,\ldots,p_n)\). Tomamos uma família de subconjuntos \(\mathcal{S}\) de \(\Gamma\), tal que \(\vazio\notin \mathcal{S}\subset 2^\Gamma\), e estamos interessados na quantidade \(\#\{A\in\mathcal{S}:A\subset \Gamma_p\}\) de membros de \(\mathcal{S}\) contidos em \(\Gamma_p\).

\begin{exemplo}
  Se \(\Gamma=\binom{[n]}{2}\), então \(\Gamma_p=G(n,p)\), onde\(p=(p,p,\ldots,p)\). Tomando os conjuntos~\({\mathcal{S}=\{\binom{U}{2}:U\subset[n],|U|=3\}}\), e nos interessa a quantidade~\(X=\#\{\text{triângulos em \gnp}\}\).
\end{exemplo}

Considere \(A\in\mathcal{S}\), \(I_A=[A\subset\Gamma_p]=\Um\{A\subset\Gamma_p\}\). Então \(X=X_{\mathcal{S}}=\sum_{A\in\mathcal{S}}I_A\).

\begin{teorema}
	\[
		\Prob(X=0)\geq \exp\left\{\frac{- \EE(X)}{1 - \max_i p_i}\right\}.
	\]
\end{teorema}

\begin{proof}
  Segue de H-FKG. Os eventos \(\{X=0\}\) e \(\{I_A=0,\;\forall A\in\mathcal{S}\}\) são iguais. Então
  \begin{align*}
    \Prob(X=0)
    & \igual \Prob (I_A=0\; \forall A\in\mathcal{S}\}\\
    & \gigual \prod\Prob(I_A=0) \qquad\text{(H-FKG)}.
  \end{align*}
Note que \(\{I_A=0\}=\{A\notin\Gamma_p\}\) é um evento decrescente.

\begin{observacao}
	\(Z_i\) variáveis \(0\)-\(1\) então
	\begin{align*} 
		\EE (Z_1Z_2) &\igual \Prob(Z_1Z_2=1),\\
		\EE(Z_i) &\igual \Prob(Z_i=1),\\ 
		\Prob(Z_1=1,Z_2=1) &\igual \Prob(Z_1=1)\Prob(Z_2=1).
	\end{align*}
\end{observacao}
Continuando, 
\begin{eqnarray}
	\Prob(X=0)	&\geq &	\prod_{I_A\in\mathcal{S}}\Prob(I_A=0) 											\nonumber \\ 
				& =	&	\prod_{A\in\mathcal{S}}\bigl(1-\Prob(I_A=1)\bigr)								\nonumber \\ 
				&\geq & \prod_{A\in\mathcal{S}} \exp\bigl\{- \Prob(I_A=1)\Bigm/1 -\Prob (I_A=1)\bigr\} 	\label{eq:desBasica} \\
				&\geq & \prod_{A\in\mathcal{S}} \exp\bigl\{- \Prob(I_A=1)\Bigm/1 -\max_i \,p_i\bigr\} 	\label{eq:maxProb} \\
				& = &	\prod_{A\in\mathcal{S}} \exp\bigl\{- \EE(I_A)\Bigm/1 -\max_i p_i\bigr\}			\nonumber \\
				&= &	\exp\bigl\{- \EE(X)\Bigm/1 -\max_i p_i\bigr\}.									\nonumber
\end{eqnarray}
onde~\eqref{eq:desBasica} segue da desigualdade~\(e^x \geq 1-x \geq e^{-x/(1-x)}\),~\(\forall x\in\RR\) e a desigualdade~\eqref{eq:maxProb} segue do fato~\(\PP(I_A = 1) = \prod_ip_i \leq \max_i p_i\).

\end{proof}

Nos preparamos agora para um teorema de Janson. Tome~\(\mathcal{S}\),~\(X=\sum_{A\in\mathcal{S}}\) e~\(\lambda=\EE(X) = \sum_{A\in\mathcal{S}} \EE(I_A)\) como acima, e ainda~\(\bar \Delta = \sum_{(A,B)}\EE(I_AI_B)\), onde a soma é sobre todos os pares \((A,B)\in \mathcal{S}\times\mathcal{S}\) com \(A\cap B\neq \vazio\). Se~\(A\) e~\(B\) são disjuntos, então são independentes.

Note que 
\begin{align*}
	\bar\Delta
		& =		\sum_{A\in\mathcal{S}}\left(\sum_{\substack{B\in\mathcal{S}\\B\cap A\neq \vazio}}\EE(I_AI_B)\right)					\\
		& =		\sum_{A\in\mathcal{S}}\EE\left(I_A\overbrace{\sum_{\substack{B\in\mathcal{S}\\B\cap A\neq \vazio}}I_B}^{Y_A}\right)	\\
		& =		\sum_{A\in\mathcal{S}}\EE(I_AY_A).
\end{align*}

Onde, para dado~\(A \in \mathcal{S}\),~\(Y_A\) conta a quantidade de~\(B\cap A\neq \vazio\) que são sorteados em~\(\Gamma_p\).
Seja 
\[
\varphi(x)=
\begin{cases}
(1+x)\log(1+x) -x &x\geq-1,\\
\infty & x< -1.
\end{cases}
\]

\begin{teorema}[Janson '90]\label{Teo:Janson:90}
Sejam \(X,\lambda, \bar\Delta\) e \(\varphi\) como acima. Então para todo \(0\leq t\leq \lambda\) temos
\begin{align*}
	 \Prob(X\leq \lambda-t)
		& \stackrel{(1)}{\leq} \exp\left\{-\frac{\lambda^2}{\overset{-}{\Delta}}\varphi(-t/\lambda)\right\} \\
		& \stackrel{(2)}{\leq} \exp\left\{-\frac{t^2}{\overset{-}{\Delta}}\right\}.
\end{align*}
\end{teorema}


\begin{observacao}\label{obs:deltabarra}
  \(\Delta\Deq\sum_{\{A,B\}}\sum\EE(I_AI_B)\) onde a soma é sobre os pares não ordenados~\({\{A,B\}\in\binom{\mathcal{S}}{2}}\), com~\(A\cap B\neq \vazio\). Então \(\bar\Delta = \sum_{(A,A)}\EE(I_A^2)+2\Delta = \lambda + 2\Delta \geq \lambda\).

	O caso importante para nós é quando~\(\Delta\) não é muito grande com respeito a~\(\lambda\) (\(\Delta = o(\lambda\))).
\end{observacao}

\begin{observacao}\label{obs:desigaldade 2}
Janson é intrinsecamente para desvios para baixo!
\end{observacao}

\begin{prova}[da desigualdade de Janson]
  Usamos a técnica de Bernstein: Markov em \(e^{uX}\) --- neste caso em \(e^{-sX}\), \(s\geq 0\). Temos \(\{X\leq \lambda -t\}=\{e^{-sX}\geq e^{-s(\lambda -t)}\EE(e^{-sX})\). Defina \(\phi(s)=\EE(e^{-sX})\), \(s\geq 0\).



Afirmamos que
\begin{equation}
  -\log \phi(s)\geq \frac{\lambda^2}{\overset{-}{\Delta}}(1 - e^{-e\deltabarra/\lambda}),\quad \forall s\geq 0.\label{eq:prova Janson *}
\end{equation}

\begin{prova}
  Para \(s=0\), temos igualdade. Vamos provar que \(-\log \phi(s))'\geq (\LD)' = \lambda e^{-s\deltabarra/\lambda}\). (Note que isto é suficiente.) Temos
  \begin{equation*}
    \phi'(s)=\EE(Xe^{-sX}) = \sum_A\EE(I_Ae^{-sX}).
  \end{equation*}
Onde a primeira igualdade não apresenta problemas pois estamos em um contexto finito (\ie, a esperança é um somatório finito). Paa \(A\in\mathcal{S}\), decompomos \(X\) como \(X = Y_A+Z_A\), onde 
\[
Y_A=\sum_{\substack{B\in\mathcal{S}\\B\cap A\neq \vazio}}I_B \quad \text{e}\quad Z_A=\sum_{\substack{B\in\mathcal{S}\\B\cap A= \vazio}}I_B
\]
em \(\{I_A=1\}\). Seja \(p_A\deq\Prob(I_A=1)\). Então \(\EE(I_Ae^{-sX})=p_a\EE(I_Ae^{-sX}|I_A=1)\). Usamos a decomposição e o fato de \(Y_A\) e \(Z_A\) serem crescentes.

\begin{align*}
\EE(I_Ae^{-sX}) 
&\gigual p_A\EE(I_Ae^{-sY_A}|I_A=1)\EE(I_Ae^{-sZ_A}|I_A=1)\\
&\igual  p_A\EE(e^{-sY_A}|I_A=1)\EE(e^{-sZ_A})\\
&\gigual p_A\EE(e^{-sY_A}|I_A=1)\phi(s).
\end{align*}
onde eliminamos o condicional pela independência, e a última linha decorre da dominância ponto a ponto. Assim, 
\begin{align*}
-\bigl(\log \phi(s)\bigr)'
&\igual  \frac{-\phi'(s)}{\phi(s)}\\
&\gigual \sum_{A\in\mathcal{S}}p_A\EE(e^{-sY_A}|I_A=1)\\
&\gigual \lambda\sum_A \frac{1}{\lambda}p_Ae^{-s\EE(Y_A|I_A=1)}\\
&\gigual \lambda\exp\left\{-s\sum_{A}\frac{p_A}{\lambda}\EE(Y_A|I_A=1)\right\}\\
&\igual \lambda e^{-(s/\lambda)\deltabarra},
\end{align*}
que implica a afirmação.\qed
\end{prova}

De~\ref{eq:prova Janson *} e pela definição de \(\phi(s)\) temos
\begin{align} \log\Prob(X\leq\lambda -t)
&\ligual \log\phi(s)+s(\lambda -t) \nonumber\\
&\ligual \frac{\-\lambda^2}{\deltabarra}(1 - e^{-s\deltabarra/\lambda)}+s(\lambda -t).\label{eq:prova Janson **}
\end{align}

Para o caso \(t=\lambda\), basta tomar \(s\to\infty\) (\(\phi(-1)=1\)). Se \(t<\lambda\), tomamos \(s=-\log(1 -t/\lambda)\lambda/\deltabarra\geq 0\), e obtemos de~\ref{eq:prova Janson **} que \(\log(\Prob(X\leq \lambda -t)\leq -\lambda^2/\deltabarra\phi(-t/\lambda)\), de onde a observação~\ref{obs:deltabarra} segue. A desigualdade~\ref{obs:desigaldade 2} segue de \(\phi(x)\geq x^2/2\) para \(-1\leq x\leq 0\).\qed
\end{prova}

\begin{teorema}[\Janson,\Luczak,\Rucinski,'90]
  \begin{enumerate}
  \item \(\Prob(X=0_\leq \exp\{-\lambda^2/\deltabarra\}\).\label{eq:JLR 1}
  \item \(\Prob(X=0)\leq \exp\{-\lambda+\Delta\}\).
  \end{enumerate}
\end{teorema}

Uma cota mais simples é \(\Prob(X=0)\leq e^{-\lambda + 2\Delta}\). Note que~\ref{eq:JLR 1} é menor ou igual a \(e^{-\lambda^2/\deltabarra}\); e \(\lambda^2/\deltabarra=\lambda^2/(\lambda+2\Delta)\geq \lambda -2\delta\).

