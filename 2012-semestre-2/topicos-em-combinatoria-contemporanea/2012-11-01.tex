\begin{resumo-aula}{1}{11}{12}
Conjuntos \mencao{quase-aleatórios} em grupos abelianos e o lema de regularidade de \Green, para \mencao{grupos abelianos} de torção artibrária.
\end{resumo-aula}

Iniciamos lembrando algumas coisas já vistas: uniformidade de subconjuntos e alguns lemas.

\section{Lembramos que\ldots}
Lembramos quando é que um subconjunto é \(\alpha\)-uniforme ou (\(\alpha\)-pseudoaleatório): Temos \(A\subset Z\); \(\|A\|_u=\sup_{\xi\neq 0}|\hat 1_A(\xi)|\). \(\alpha\)-uniforme: \(\|A\|_u\leq \alpha\).

Também havíamos definido o operador \(\sigma\): Dados \(A_1,\ldots,A_n\subset Z\);
\begin{align*}
  \sigma:A_1\times\cdots\times A_n &\mapsto Z\\
(a_1,\ldots,a_n) &\mapsto \sum_{j=1}^n a_j.
\end{align*}
Esse operador tem a propriedade \(|\sigma^{-1}(x)|=\#\{(a_1,\ldots,a_n)\in A_1\times\cdots\times A_n: \sum_1^n a_j=x\}\).

Ademais, vimos o lema \ref{lem:limitante para densidade de somas}: para \(n\geq 3\) e \(A_1,\ldots,A_n\subset Z\), 
\[
\left|\frac{|\sigma^{-1}(x)|}{|Z|} -\bbp(A_1)\cdots\bbp(A_n)\right|
\leq
\|A_1\|_u\cdots\|A_{n-2}\|_u\sqrt{\bbp_Z(A_{n -1})\bbp_z(A_n)},
\]
que provamos usando a transformada de \Fourier\ e \CauchySchwarz.

Exemplos de estruturas \mencao{pseudoaleatórias} ou \mencao{quase-aleatórias}: grafos, subconjuntos de \(\bbz/n\bbz\), torneios, hipergrafos. Nomes associados: \alerta{verificar!} \Erdos, Frankel, R\H odl, Thomasson, Wilson, Chung, Graham.

Subconjuntos quase-aleatórios de grupos abelianos: Chung \&\ Graham: \(A\subset Z\) é quase-aleatório se \(\|A\|_u=\littleo{1}\), mas isso não faria sentido. O que acontece é que, em geral, temos um família de objetos. Mais precisamente, temos \((A_n)_{n\geq 1}\), com \(A_n\subset Z_n\), e \(|Z_n|\to\infty\) (\(n\to\infty\)). (Em geral, o termo ``quase-aleatório'' tem um uso mais específico e preciso, enquanto o termo pseudoaleatório é usado mais vagamente.) Veremos várias propriedades desses conjuntos.

\section{Notação}

Denotamos até agora a função característica do conjunto \(A\) por~\(1_A\). Neste capítulo, faremos uso da notação \(A(x)=1_A(x) = [x\in A]\) para a função que assume o valor \(1\) em elementos de \(A\), e \(0\) caso contrário.

O grafo \(G_A=G_A^+\) tem conjunto de vértices \(V(Z_A)=Z\) e conjunto de arestas \(E(G_A)=\bigl\{\{x,y\}:x+y\in A\bigr\}\). (Note que \(G_A\) pode ter ``laços'', \ie, \(x=y\) será permitido). Outro comentário: um grafo de Cayley é definido analogamente, com base na relação \(x -y\in A\) em vez da soma.% todo: verificar o nome!

O grafo \(G=G^N\) é dito \(\eta\)-uniforme se \(\forall S\subset V(G)\), \(|S|\geq \eta N\) temos \(\nedges(G[S])=(1\pm\eta)\binom{|S|}{2}p\), onde \(p=\nedges(G)\binom{N}{2}^{-1}\).

\begin{definicao}
  \G\ é \defi{quase-aleatório} se \G\ é \littleo{1}-uniforme. Mais precisamente, \((G_n)_{n\geq 1}\) é quase-aleatório se \(G_n\) é~\(\eta=\eta(n)\) uniforme para \(\eta\to 0\) quando~\(n\to \infty\).
\end{definicao}

\section{\texorpdfstring{Várias propriedades para $A\subset Z$}{Várias propriedades para A subconjunto de Z}}

Pomos \(\displaystyle p=\bbp_Z(A)=\frac{|A|}{|Z|}\).

\begin{definicao}[WT --- translação fraca, \emph{weak translation}]
Para \qt\ (quase todo: \(1-\littleo{1}\)) \(x\in Z\),
\[
\frac{|A\cap (A+x)|}{|Z|} = p^2 + \littleo{1}.
\]
\end{definicao}

\begin{definicao}[ST --- translação forte, \emph{strong translation}]
Para todo \(B\subset Z\) e para \qt\ \(x\in Z\),
\[
\frac{|A\cap (B+x)|}{|Z|} = pq + \littleo{1},
\]
onde \(q=\bbp_Z(B)\).
\end{definicao}

\begin{definicao}[P(2) --- 2-pattern]
  Para \qt\ \(u_1,u_2\in Z\),
\[
\frac{1}{|Z|}\sum_{x\in Z}A(x+u_1)A(x+u_2)=p^2+\littleo{1}.
\]
\end{definicao}

\begin{definicao}[P(k) --- \(k\)-pattern]
  Para \qt\ \(u_1,u_2,\ldots,u_k\in Z\),
\[
\frac{1}{|Z|}\sum_{x\in Z}\prod_{j=1}^kA(x+u_j)=p^k+\littleo{1}.
\]
\end{definicao}

\begin{definicao}[R\((2)\) --- 2-representações]
para \qt\ \(x\in Z\),
\[
\frac{1}{|Z|}\sum_{\substack{u_1+u_2=x\\(u_1,u_2)}}A(u_1)A(u_2)=p^2 +\littleo{1}.
\]
\end{definicao}

Na definição acima, note que cada par \(u_1+u_2=x\) aparece \(\sigma(u_1,u_2)=x\), e então o somat'rio é \(\sigma^{-1}(x)\), cujo valor ``esperado'' é
\[
\frac{|\sigma^{-1}(x)}{|Z|}=p^2 +\littleo{1}.
\]

\begin{definicao}[R\((k)\) --- \(k\)-representações]
para \qt\ \(x\in Z\),
\[
\frac{1}{|Z|^{k -1}}\sum_{\substack{(u_1,\ldots,u_k)\\u_1+\cdots+u_k=x}}A(u_1)\cdots A(u_k)=p^k +\littleo{1}.
\]
\end{definicao}

\begin{definicao}[EXP --- somas exponenciais]
  \(\|A\|_u=\littleo{1}\).
\end{definicao}

\begin{definicao}[\defi{Grafo quase-aleatório}]
  O grafo \(G_A=G_A^+\) é quase-aleatório.
\end{definicao}

\begin{definicao}[\(\widetilde{C(2t)}\) --- \(2t\)-circuitos em \(G_A\)]
\(G_A\) tem \(C(2t)\).
\end{definicao}

\begin{definicao}[\(C(2t)\) --- \(2t\)-circuitos]
\[
\frac{1}{|Z|^{2t}}
\sum_{x_1,\ldots,x_{2t}} A(x_1+x_2)A(x_2+x_3)\cdots A(x_{2t -1}+x_{2t})A(x_{2t}+x_1)=p^{2t}+\littleo{1}.
\]
\end{definicao}

\begin{definicao}[Densidade]
Para todo \(B\subset Z\),
\[
\frac{1}{|Z|^2}
\sum_{(x,y)\in Z\times Z} B(x)B(y)A(x+y)=pq^2 + \littleo{1},
\]
onde \(q=\bbp_Z(B)=|B||Z|^{-1}\).
\end{definicao}

\begin{teorema}
Fixe \(A\subset Z\). Todas as propriedades acima para~\(A\) são equivalentes.
\end{teorema}

Ademais, o lema de regularidade de \Green: \(\forall A\subset Z:\exists H\subset Z\), com  \(|Z|/|H|=\littleo{1}\) tal que \qt\ \(x\in H^\perp\) é regular. E, assim sendo, a equivalência entre as propriedades nos dá informação estrutural sobre tal conjunto.