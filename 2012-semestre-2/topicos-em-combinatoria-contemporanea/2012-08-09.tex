\begin{resumo-aula}{9}{8}{2012}
Cotas exponenciais para grandes desvios. \Chernoff, Harris-FKG e afins. (Truque de \Bennet\ da exponencial.)
\end{resumo-aula}

\subsection{Desigualdades Elementares} 

\subsubsection{Poisson}
Para \(X\sim\Po(\lambda)\), se trabalhamos com \(k>\lambda\), podemos obter uma \mencao{cota} (\mencao{bound}) para
\begin{align*}
\Prob(X\geq k)
&\igual \sum_{t\geq k} e^{-\lambda}\lambda^t/t!\\
%&\ligual \left[e^{-\lambda}\Bigm/\left(1 - \frac{\lambda}{k}\right)\right]\frac{\lambda^k}{k!} \\
&\ligual e^{-\lambda}\frac{\lambda^k}{k!}\Bigm/\left(1 - \frac{\lambda}{k}\right) \\
&\igual \Prob(X=k)\Bigm/\left(1 -\frac{\lambda}{k}\right).
\end{align*}
Justificativa da desigualdade: \(\displaystyle\frac{\lambda^t}{t!}\Bigm/\frac{\lambda^{t -1}}{(t -1)!} = \frac{\lambda}{t}\leq \frac{\lambda}{k}\), e então a soma de tais termos pode ser majorada por uma P.G.\ de razão \(\lambda/k\), cuja soma infinita a partir do \(k\)-ésimo termo é~\(\displaystyle\frac{\lambda^k}{k!}\Bigm/\left(1 -\frac{\lambda}{k}\right)\).

\subsubsection{Binomial}

\(X\sim\Bi(n,p)\). Então \(\Prob(X\geq k)\leq \binom{n}{k}p^k\leq \left(\frac{enp}{k}\right)^k\), e a cota é exponencial se \(k>enp=e\EE(X)\).

\subsubsection{Hipergeométrica}

\(X\sim\Hyp(n,b,d)\). Então \(\Prob(X=k)=\binom dk\binom{n -d}{b -k}\bigm/\binom nb\), e --- de modo análogo ao que acabamos de fazer --- \(Prob(x\geq k) \leq \binom{d}{k}\left(\frac{b}{n}\right)^k\).

\begin{exercicio}
  Continue o desenvolvimento como feito para o binomial, e chegue a~\({\Prob(X\geq k)\leq \left(\frac{ebd}{nk}\right)^k}\).
\end{exercicio}

\subsection{Cotas de \Chernoff}

Seja \(X\sim\Bi(n,p)\).

\begin{teorema}
  Seja \(\lambda=\EE(X)=np\) e \(t\geq 0\). Então
  \begin{align}
    \Prob(x\geq \lambda + t) 
    &\ligual \exp\left\{-\frac{t^2}{2(\lambda + t/3)}\right\}, \label{eq:Chernoff cota superior} \text{ e}\\
    \Prob(x\leq \lambda - t) 
    &\ligual \exp\left\{-\frac{t^2}{2\lambda}\right\}. \label{eq:Chernoff cota inferior}
  \end{align}
\end{teorema}

\begin{corolario}
  Nas mesmas condições, \(0\leq \varepsilon\leq 3/2\), temos
\[
\Prob\bigl(|X-\lambda|\geq \varepsilon\lambda\bigr)\leq 2\exp\left\{-\frac{1}{3}\varepsilon^2\lambda\right\}.
\]
\end{corolario}

\begin{prova}
  Tome \(t=\varepsilon\lambda\) em \ref{eq:Chernoff cota superior} e \ref{eq:Chernoff cota inferior}. Note que 
\[
\frac{\varepsilon^2\lambda^2}{2(\lambda + \varepsilon\lambda/3)}\geq \frac{1}{3}\varepsilon^2\lambda,
\]
pois \(\varepsilon\leq 3/2\). Basta agora somar \ref{eq:Chernoff cota superior} e \ref{eq:Chernoff cota inferior}. \qed
\end{prova}

Vamos assumir  aqui que \(X\) é uma soma de variáveis indicadoras. Mas (exercício da lista) o resultado vale mesmo para \(X=\sum_i X_i\), em que \(X_i\) são v.a.'s com~\(\EE(X_i)=p\) e \(X_i\in[0,1]\).

Usamo o ``truque de \Bennet'', bem antigo: \(Y=e^{uX},\; u\in\RR\). Seja \(u\in\RR\). Considere a desigualdade de \Markov\ para \(Y=e^{uX}\geq 0\). Se \(u>0\), temos 
\begin{equation}
\Prob(X\geq \lambda+t)=\Prob(\underbrace{e^{uX}}_Y \geq e^{u(\lambda+t)})\leq e^{-u(\lambda+t)}\EE(e^{uX}).\label{eq:3:Bennet u>0}
\end{equation}

Para \(u<0\), temos 
\begin{equation}
  \Prob(X\leq \lambda -t)=\Prob(e^{uX}\geq e^{u(\lambda -t)})\leq e^{-u(\lambda -t)}\EE(e^{uX}).\label{eq:4:Bennet u<0}
\end{equation}

Queremos estimar \(\EE(e^{uX})\). Como \(X=\sum_{i=1}^nX_i\) com \(X_i\sim\Be(p)\) \emph{independentes}, 
\begin{equation}
\EE(e^{uX})
=\EE(e^{u\sum X_i})=\prod_{i=1}^n\EE(e^{uX_i})=\prod_{i=1}^n(1 -p + pe^u)= (1 -p +pe^u)^n.\label{eq:5:aproximacao}
\end{equation}

(Se \(X_i\sim\Be(p_i)\), temos \(\EE(e^{uX})\leq (1 -\bar p+\bar pe^u)^n\), pela desigualdade entre média geométrica e aritmética, \(\bar p=\sum p_i/n\). Ou tirando logaritmo, observando que a função é côncava e aplicando a desigualdade de \Jensen). De \ref{eq:3:Bennet u>0} e~\ref{eq:5:aproximacao}, temos
\begin{equation}
	\Prob(X\geq\lambda+t)\leq e^{-u(\lambda+t)}(1 -p + pe^u),\quad 
	\text{ para todo \(u\geq 0\).}\label{eq:6:desigualdade}
\end{equation}

Considere \(u\) com \(e^u=(\lambda+t)(1 -p)\!\bigm/\!(u -\lambda -t)p\). Note que essa expressão possui valor maior ou igual a 1 \verif. Com este \(u\), a desigualdade~\ref{eq:6:desigualdade} torna-se
\[
\Prob(X\geq \lambda+t)\leq
\left(
\frac{\lambda}{\lambda+t}
\right)^{\lambda+t}
\left(
\frac{n -\lambda}{n -\lambda -t}
\right)^{n -\lambda -t},\qquad 0\leq t\leq n -\lambda.
\]

Considere \(\varphi(x)=(1+x)\log(1+x) -x\) para \(x>-1\). Para \(x>-1\) e \(\varphi(x)=\infty\) para \(x<-1\) e \(\varphi(-1)=1\).

\begin{lema}
  \begin{align}
    \varphi(x) &\gigual {x^2\over2(1+x/3)} \text{ para \(x\geq0\), e}\label{eq:1:lema phi}\\
    \varphi(x) &\gigual {x^2\over2} \text{ para \(x\leq 0\)}. \label{eq:2:lema phi}
  \end{align}
\end{lema}

O logaritmo do lado direito (\LD) de \ref{eq:6:desigualdade} é 
\begin{align*}
-\lambda\bigl[(1 &+ t/\lambda)\log(1+t/\lambda) - t/\lambda\bigr]\\
&\quad\quad- (n -\lambda)\left[\left(1 - \frac{t}{n -\lambda}\right)\log\left(1 -\frac{t}{n - \lambda}\right)-\frac{t}{n\lambda}\right]\\
&=\quad -\lambda\varphi(t/\lambda) - (n -\lambda)\varphi\bigl(-t/(n -\lambda)\bigr)\\
&=\quad -\lambda\varphi(t/\lambda).
\end{align*}

Assim, \ref{eq:6:desigualdade} implica que \(\Prob(X\geq \lambda+t)\leq e^{-\lambda\varphi(t/\lambda)}\).

Temos \(\lambda\varphi(t/\lambda)\geq t^2/\lambda\bigm/2(1+t/\bigl(3\lambda)\bigr)=t^2/\lambda\bigm/2(\lambda+t/3)\), de onde \ref{eq:1:lema phi} segue.

De forma análoga, podemos provar 
\[
\Prob(X\leq \lambda -t)\leq \exp\bigl\{-\lambda\varphi(-t/\lambda) -(n-\lambda)\varphi(t/(n -\lambda))\bigr\},
\]
e a desigualdade \ref{eq:2:lema phi} segue.

O lema prova-se derivando algumas vezes.

Uma situação interessante: \(X\) é contagem. Qual a probabilidade de ser zero? (Desvio para baixo.) Se a dependência entre v.a.'s não é muito grande, então podemos usar Janson.

\section{Desigualdade Harris-FKG}
Contexto geral: \(\Gamma\) conjunto finito; \(0\leq p\leq 1\). Escrevemos \(\Gamma_p\) para o subconjunto aleatório de \(\Gamma\) com \(\Prob (x\in \Gamma_p)=p\) e todos estes eventos independentes. Mais geralmente, usamos a notação \(\Gamma_{p_1,\ldots,p_n}\) se \(\Gamma=[n]\) e \(i\in \Gamma_p\) com probabilidade \(p_i\).

\begin{definicao}
Uma função \(f:2^\Gamma\to\RR\) é \defi{crescente} (\defi{decrescente}) se \(f(A)\leq f(B)\) sempre que \(A\subset B\) (respectivamente \(f(A)\geq f(B)\) sempre que \(A\supset B\)).
\end{definicao}

\begin{teorema}[Harris-FKG]
  Se as variáveis aleatórias \(X_1\) e \(X_2\) são ambas crescentes ou ambas decrescentes, então temos que~\({\EE(X_1X_2)\geq \EE(X_1)\EE(X_2)}\).
\end{teorema}

\begin{exemplo}
  Tome \(\Gamma=\binom{[n]}{2}\). Então \(\Gamma_p=G(n,p)\). 
  \begin{align*}
    X_1 &\igual [\text{\gnp\ é conexo}]\\
    X_2 &\igual [\text{\gnp\ é hamiltoniano}]\\
    X_3 &\igual [\text{\gnp\ não é \(3\)-colorível}]\\
    X_4 &\igual [\text{\gnp\ não é planar}]\\
        \vdots
  \end{align*}
\end{exemplo}
