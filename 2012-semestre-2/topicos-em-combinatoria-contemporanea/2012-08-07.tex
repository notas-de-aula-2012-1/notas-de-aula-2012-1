\begin{resumo-aula}{7}{8}{2012}
Revisão de probabilidade, distribuições básicas. Variáveis indicadoras, algumas propriedades da esperança e desigualdades elementares.
\end{resumo-aula}

\section{Fatos elementares de probabilidade}

Trabalharemos com espaços discretos \((\Omega_n,\Prob_n)\). Temos \Omg\ finito em geral (por vezes enumerável). A função \(\Prob\) é \(\Omega \mapsto [0,1]\). Vale ainda que \(\sum_\omega\Prob(\omega)=1\). Uma \defi{variável aleatória} é uma função \(X:\Omega\mapsto\RR\). A \defi{esperança} da variável aleatória \(X\) é \(\EE(X)\deq\sum_{\omega\in\Omega}X(\omega)\Prob(\omega)=\sum_x x\Prob(X=x)\).

\subsection{Linearidade da Esperança}

É um fato sobre somas \(\EE\bigl(\sum_i a_i X_i\bigr)=\sum_i a_i\bigl(\EE X_i\bigr)\).

\subsection{Variância}

Definimos a \defi{variância} como \(\Var (X)\deq\sigma^2 (X) = \EE\bigl[ (X -\EE (X))^2\bigr] = \EE \bigl(X^2\bigr) - \bigl(\EE X\bigr)^2\).

\subsection{Independência}

As variáveis aleatórias \(X\) e \(Y\) são \defi{independentes} se, para todo \(x\) e para todo \(y\), vale que \(\Prob (X=x,Y=y)=\Prob(X=x)\Prob(Y=y)\). Em geral, para  \(n\geq2\) e v.a's~\((X_{\lambda_i})_{\lambda\in\Lambda}\), dizemos que são independentes se para todo \(\lambda_1,\ldots,\lambda_n\), temos para todo \(x_1,\ldots,x_n\)
\[
\Prob(X_{\lambda_i}\!=x_i \;\forall i)=\prod_i\Prob(X_{\lambda_i}=x_i).
\]

\begin{exercicio}
  Mostre que \(\EE(XY)=\EE(X)\EE(Y)\) se \(X\) e \(Y\) são independentes.
\end{exercicio}

\subsection{Variáveis Indicadoras}

Muitas vezes temos uma coleção \(\xi=(E_\lambda)\) de eventos e queremos saber quantos \(E_\lambda\) ocorrem.
\[
X = \# \bigl\{E_\lambda \text{ que valem }\bigr\} = \sum_\lambda \Um_{E_\lambda}.
\]

Note que \(\EE X = \sum_\lambda\EE(\Um_{E_\lambda})=\sum_\lambda\Prob(E_\lambda)\).

\(\Var X = \sum_{(\lambda,\lambda')}\Cov(\Um_{E_\lambda},\Um_{E_{\lambda'}},)\), onde \(X=\sum_\lambda \Um_{E_\lambda}\) e também
\begin{align*}
\Cov(\Um_{E_\lambda},\Um_{E_{\lambda'}},) &\igual\underbrace{\EE(\Um_{E_\lambda}\cdot\Um_{E_{\lambda'}},)}_{\displaystyle\Prob(E_\lambda\cap E_{\lambda'})}
- \EE(\Um_{E_\lambda})\EE(\Um_{E_{\lambda'}},)\\
&\igual \EE(\Um_{E_\lambda\cap E_{\lambda'}}) - \Prob(E_\lambda)\Prob(E_{\lambda'}).
\end{align*}

\begin{exercicio}
  O que acontece se \(E_\lambda\) e \(E_{\lambda'}\) são independentes?
\end{exercicio}

\subsection{Desigualdades Básicas}

Para \(X\geq 0\) variável aleatória.

\Markov: Para todo \(t>0\), 
\[
\Prob(X\geq t)\leq \EE(X)\bigm/ t.
\]

Muitas vezes em combinatória se quer contar e, digamos, mostrar que com frequência a quantidade contada é zero (com alta probabilidade nenhum dos eventos de alguma coleção ocorre).
  \newcommand{\xisene}{\ensuremath{X^{(n)}}}
\begin{exemplo}
  \(X\geq 0\) inteiro. Então \(\Prob(\xisene>0)=\Prob(\xisene\geq 1)\leq \EE(\xisene)\). Se limitamos a esperança superiormente por zero, por exemplo\ldots\ (Observação: um caso bom aqui é quando temos a informação de que há eventos independentes.)

Muitas vezes temos um parâmetro \(n\) e estamos interessados em \(n\to \infty\). Provaremos que \(\EE(\xisene)\to0\) quando \(n\to\infty\) e concluiremos que \(\xisene=0\) ``(assintoticamente) quase certamente''.
\end{exemplo}


\Chebyshev: para todo \(t>0\) vale \(\Prob\bigl(|X -\EE X| \geq t\bigr) \leq \Var (X) \bigm / t^2\).

\begin{prova}
  Aplicar a desigualdade de \Markov\ para a variável \(Y=\bigl(X - \EE (X)\bigr)^2\)\qed
\end{prova}

\begin{observacao}
  Tomando \(t=\EE X\) temos \(\Prob(X=0)\leq \Prob\bigl(|X-\EE (X)|\geq \EE(X)\bigr)\leq \Var X /\EE (X)^2\).
\end{observacao}

\begin{exercicio}
  Use \CauchySchwarz\ para demonstrar que
\[
\Prob(X=0)\leq \frac{\Var X}{\EE (X)^2 + \Var (X)}=\frac{\Var X}{\EE (X^2)}.
\]
\end{exercicio}

Para \(X\geq 0\) inteiro, temos \(\Prob(X\geq 1)\geq \EE(X)^2\bigm/ \EE(X^2)\).

\section{Concentração Básica}

Embora a esperança seja uma estatística bastante simples, ela por vezes é o bastante para que sejamos capazes de afirmar coisas sobre a v.a.\ com alta probabilidade. Suponha que tenhamos um parâmetro \(n\) e que \(n\to\infty\). suponha ainda \(\Var(\xisene)\ll \EE(\xisene)^2\). (A partir daqui diremos \(X\deq\xisene\).) então, temos \(\Prob\bigl(|X-\EE(X)|\geq \varepsilon\EE(X)\bigr)\leq \Var(X) \bigm/ \bigl(\varepsilon^2\EE(X)^2\bigr) \ll 1/\varepsilon^2\). Portanto \defi{a.q.c.}\ (\mencao{assintoticamente quase certamente}), temos \(X = (1\pm\varepsilon)\EE(X)\), desde que consigamos estimar de algum modo a variância.

\begin{observacao}
  Grafo aleatório \gnp\ tem conjunto de vértices, digamos, \(V=[n]=\{1,2,\ldots,n\}\), e \(\{i,j\}\) é aresta com probabilidade \(p\). Este é o espaço \(\Omega = \{0,1\}^{\binom{n}{2}}\), e a probabilidade de um dado grafo \(G\), com número de arestas \(e(G)\), é \(\Prob(G)=p^{e(G)}(1 -p)^{\binom{n}{2}-e(G)}\).
\end{observacao}

\begin{exercicio}
  Seja \(X=\#\bigl\{W\subseteq [n]\bigr\}\), com \(|W|=4\), induzindo 6 arestas. Calcule a esperança, a variância, e veja se há concentração. Ademais, experimente com \(p=p(n)\) para funções como \(1/n,\: 1/n^2,\:1/\!\sqrt n,\: \ldots,\: k\).
\end{exercicio}

\section{Distribuições Básicas}

\(X\sim \Bi (n,p)\) é a distribuição \defi{Binomial}: \(X\) é o número de caras obtidas em \(n\) lançamentos independentes de uma moeda --- cada um com probabilidade de ``cara'' igual a \(p\). Se \(X\) é uma v.a.\ binomial, então pode ser escrito como \(X=\sum_{i=1}^n X_i\), com \(X_i\sim\Be(p)\), cada um uma \defi{Bernoulli} (um lançamento da moeda). Temos então \(\Prob(X=k)=\binom{n}{k}p^k(1 -p)^{n -k}\) e ainda \(\EE(X)=np\).

Definimos o \defi{\(r\)-ésimo momento} \(\EE_r(X)\Deq \EE((X)_r) = \EE\bigl(X(X -1)\cdots(X -r +1)\bigr)\).


Uma  possível interpretação para o momento tem a ver com arranjos de \(r\) elementos de um subconjunto \(S\subseteq [n]\). Mais precisamente, podemos denotar \((X)_r = \sum_R X_R\), onde a soma é sobre todo
\[
R\in \bigl([n]\bigr)_r=\bigl\{(a_1,\ldots,a_r):a_i\in[n]
\text{ e todos os \(a_i\)'s são distintos}\bigr\},
\]
e \(X_R=[a_i\in S,\:\forall i]\). (Note que não é apropriado usar \(X^r\) --- o essencial e considerarmos o vetor com elementos distintos (\(p^r\)).

O \defi{segundo momento} é \(\EE_2(X)=\EE\bigl(X(X -1)\bigr)=\EE(X^2) -\EE(X)\). Obtemos, se \(np\to\infty\), que \(\Var(X)=np(1 -p) \ll \EE(X)^2 = (np)^2\), \mencao{concentração}.

Uma variável aleatória inteira apresenta distribuição de \defi{Poisson}, denotada \(X\sim \Po(\lambda)\), com parâmetro \(\lambda>0\), se satisfaz \(\Prob(X=k)\Deq e^{-\lambda}\lambda^k/k!\). Essa distribuição aparece quando buscamos, por exemplo, \(K^4\) em \gnp, onde a notação \(K^n\) representa o grafo completo com \(n\) vértices. Se \(X\) é Poisson, então \(\EE_r X = \lambda^r\).

\begin{observacao}
  \(\Bi(n,p)\dto \Po(\lambda)\) se \(np\to\lambda>0\) conforme \(n\to\infty\), \(\lambda\) constante. (Com certo abuso de notação,  \(\Prob(\Bi(n,p)=k)\to e^{-\lambda}\lambda^k/k!\).)
\end{observacao}

\defi{Hipergeométrica}. Seja \(B\subseteq [n]\) com \(b\) elementos, e \(D\) sorteado uniformemente ao acaso entre os possíveis \(\binom{n}{d}\) subconjuntos de \(d\) elementos. então \(X=\#\bigl\{D\cap B\bigr\}\) é uma variável aleatória com distribuição hipergeométrica, o que denotaremos por \(X\sim \Hyp(n,b,d)\). Verifique que
\[
\Prob(X=k)=\binom bk\binom{n -b}{d -k}\Bigm/\binom nd
=\binom dk\binom{n -d}{b -k}\Bigm/\binom nb, 
\quad \text{e} \quad 
\EE(X)=bd/n.
\]
