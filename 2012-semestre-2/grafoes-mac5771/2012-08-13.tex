\begin{resumo-aula}{13}{8}{2012}
Falamos de \mencao{flores}, \mencao{caules} e \mencao{botões} em emparelhamentos --- noções envolvidas na prova do algoritmo de Edmonds para encontrar emparelhamentos. A discussão introduz o jargão necessário para o próximo assunto, a decomposição de Edmonds-Gallai. Em particular, provamos um teorema sobre a \mencao{contração de botões}.
\end{resumo-aula}

\begin{observacao}
  Seja \(B\) alguma popriedade sobre grafos. Por vezes dizemos que um grafo
  \G\ é \defi{hipo}-\(B\) se \G\ não possui a propriedade, mas para
  qualquer vértice \vv\ de \G, se removemos \vv, então o grafo \(G-v\)
  resultante apresenta a propriedade~\(B\).
\end{observacao}

\begin{teorema}[Decomposição de Edmonds-Gallai]
  Dado um grafo \gve, sejam
  \begin{align*}
    D_G &\deq \{v\in V : \text{existe emparelhamento máx.\ em
      \G\ que não cobre \vv} \},\\
    A_G &\deq \{v\in V\setminus D_G: \text{\vv\ é adjacente a algum
      vértice de \(D_G\)}\},\\
    C_G &\deq V\setminus\bigl(D_G \cup A_G\bigr).
  \end{align*}
Então
\begin{enumerate}[a)]
\item \(S=A_G\) atinge o mínimo no lado direito da Fórmula de
  Tutte-Berge \ref{eq:formula de Tutte-Berge};
\item \(C_G\) é a união de componentes pares de \(G-A_G\);
\item \(D_G\) é a união de componentes ímpares de \(G-A_G\);
\item Todo componente ímpar de \(G-A_G\) é hipo-emparelhável.
\end{enumerate}
\end{teorema}

\begin{figure}
\begin{center}
  \faltafigura
  \caption{Decomposição de Edmonds-gallai de um grafo. Note que a
    deficiência é \(\deficiencia G=\max\bigl\{0,c\bigl([D_G]\bigr)-|A_G|\bigr\}\), e o
    tamanho do emparelhamento máximo é \(\emp G = \bigl(|V| +
    |A_G| - c\bigl([D_G]\bigr)\bigr)\bigm/ 2\).}
\end{center}
\end{figure}

Para encontrar o emparelhamento, faremos uso de caminhos
alternantes. A ideia básica é partir de algum emparelhamento, e
construir uma floresta. Partimos de algum vértice não coberto (raiz), e
``aumentamos'' a floresta acrescentando a cada passo dois vértices
emparelhados. Nesse processo, rotulamos os vértices da árvore. Quando
não pudermos prosseguir, o algoritmo termina, e as classes de vértices
definidas pelos rótulos (ou sua ausência) definem as
componentes da \mencao{decomposição de Edmonds-Gallai}. 

A rotulação dos vértices na floresta é feita de modo a que (caso o
grafo original seja uma árvore), vértices que estejam a uma distância
ímpar da raiz sejam chamados de \even, e os demais sejam rotulados
\odd. Note que vértices fora da floresta não são rotulados. (Veremos
que a equivalência será \(\{\text\odd\}=S\), \(\{\text\even\}=D\) e
\(\{\text{demais vértices}\}=C\).)

Dificuldades aparecem quando o grafo apresenta circuitos ímpares, que são os
\mencao{botões} de que falaremos. A abordagem proposta por Edmonds em
seu artigo \cite{} é a de que é possível, quando encontrado um
circuito, contraí-lo em um supernó, e prosseguir. Sendo assim, um lema
que gostaríamos de demonstrar é algo na direção de

\begin{quote}
  Se contraimos um circuito ímpar (botão), do grafo, e consideramos o grafo
  com supernó resultante, então a partir do emparelhamento máximo para
  o grafo resultante, podemos obter um emparelhamento máximo para o
  grafo original.
\end{quote}

% Decomposição em orelhas ``simpifica'' o algoritmo?
% cite!

% Conrad e Poppe (?):
% cada circuito leva a uma duplicação de alguns nós, simulando o
% circuito.

No livro do \Lovasz\ há um algoritmo, usando orelhas, de
complexidade~\bigo{n^3}. Existe algoritmo \bigo{\sqrt{n}m}, de vários
autores \cite{}, para o caso em que maximiza a cardinalidade do
emparelhamento (em oposição a maximizar o \emph{peso} das arestas no
emparelhamento, caso em que a complexidade talvez seja outra).

Para as próximas definições, considere um emparelhamento \M\ no grafo
\gve.

\begin{definicao}
  Uma \defi{\M-flor} é um \emph{passeio \M-alternante}
  \((v_0,v_1,\ldots,v_t)\) indexado de forma que
  \((v_{2k-1},v_{2k})\in M\) e   \((v_{2k},v_{2k+1})\notin M\),
      satisfazendo
      \begin{enumerate}[a)]
        \item \(v_0\in X \deq\{v: \text{\vv\ não é coberto por \M}\}\),
        \item \(v_0,\ldots, v_{t -1}\) são distintos,
        \item \(t\) é ímpar,
        \item \(v_t=v_i\) para algum \(i\) ímpar.
      \end{enumerate}
\end{definicao}

A parte da \M-flor de \vindice0\ a \vindice i é chamada \defi{caule}
(``\defi{stem}'') e a parte de \vindice i a \vindice t é chamada de
\defi{botão} (``\defi{blossom}'').
Dizemos que \vindice 0 é a \defi{raiz} da flor ou do botão.

\begin{definicao} (Contração de um botão.) Considere o grafo \gve,
  emparelhamento \M, e um \M-botão \B. O grafo \defi{\(G/B\)}
  (\G\ \defi{contraído} de \B) com emparelhamento \mencao{\(M/B\)}
  definido como segue
  \begin{itemize}
  \item \(V(G/B)=(V\setminus B)\cup\{b\}\), onde \(b\notin V\) é um vértice novo.
  \item 
    \(E(G/B)=
    \bigl(E\setminus\{e\in E:e\in B\}\bigr)
    \,\cup\, \bigl\{vb:v\in V(G/B), vz \in E(G), z\in B \bigr\}\),
  \item \(M/B = M \setminus \{e\in E:e\in B\}\).
  \end{itemize}
\end{definicao}

\begin{teorema}[2.11 na lousa]\label{teo:2.11}
Seja \gve\ um grafo, \M\ um emparelhamento em \G\ e seja \B\ um
\M-botão. Então \M\ é um emparelhamento máximo em \G\ se e somente se
\(M/B\) é um emperelhamento máximo em \(G/B\).
\end{teorema}

\begin{prova}
  Vamos provar que existe um caminho \M-aumentador em \G\ se e somente
  se existe um caminho \(M/B\)-aumentador em \(G/B\).
	
  Para todo vértice \(v\) de \B, denote por \(P_v\) o caminho de
  comprimento par que vai da raiz de \B\ até \(v\). 
  
  Assim, observe que se existe um caminho \M-aumentador  \(P\) em
  \G\ contendo arestas de \B\ então existe um caminho \M-aumentador
  com extremo na raiz de \B.
  Tome \vu\ um extremo de \(P\) diferente da raiz de \B\ e \vv\ o
  primeiro vértice do botão em \(P\) quando seguimos \(P\) partindo de
  \vu.
  Seja \(Q\) o subcaminho de \(P\) de \vu\ até \vv.
  O caminho procurado é dado pela união de \(Q\) com \(P_v\).
  
  Observe que a aresta de \vv\ em \(Q\) não é aresta de \M\ e,
  portanto, \((Q\cup P)/B\) é um caminho \(M/B\)-aumentador em
  \(G/B\).
  
  Suponha então que existe um caminho \(M/B\)-aumentador \(P\)
  em~\(G/B\).
  Se \(P\) não contém \(b\) então \(P\) é um caminho \M-aumentador
  em~\G.
  Se \(P\) contém \(b\) então seja \vu\ um extremo de \(P\) diferente
  da raiz de \B\ e \(v \in V(G)\) o primeiro vértice do botão
  em~\(P\).
  Como antes, tome \(Q\) o subcaminho de \(P\) de \vu\ a \vv\ e então
  \(Q \cup P_v\) é um caminho \M-aumentador em \G. 	
\end{prova}


\begin{observacao}
  O teorema \ref{teo:2.11} não diz que se temos um emparelhamento
  máximo \(M^\star\) em \(G/B\) então basta adicionar
  \((|B| -1)/2\) arestas (de um emparelhamento quase perfeito de \B)
  a \(M^\star\) para obter \(\hat M\), um emparelhamento máximo em \G.
\end{observacao}

\begin{exercicio}
  Dê um exemplo de um grafo \G, um emparelhamento \M\ e um \M-botão
  tal que um emparelhamento máximo \(M^\star\) em \(G/B\) não leva a
  um emparelhamento máximo e m\G. (Observe que isto não contradiz o
  teorema~\ref{teo:2.11}.)
\end{exercicio}
