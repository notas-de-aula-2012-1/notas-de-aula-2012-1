\begin{resumo-aula}{8}{8}{2012}
Generalizações dos resultados da aula passada. Teorema de Ore,
\mencao{defeito} e \mencao{deficiência} em emparelhamentos. Fórmulas
de \mencao{Berge} e \mencao{Tutte-Berge}.
\end{resumo-aula}

A \emph{maior diferença} \(|S|-|\adj{S}|\) é uma medida da
\emph{deficiência do grafo} --- que definiremos logo em breve. Em
função dela podemos calcular o tamanho do emparelhamento máximo. Em
1955, Ore publicou uma ``versão defectiva'' do \mencao{teorema de Hall} para
grafos bipartidos. % importante mencao porque fala da versão
                   % generalizada.
Em 1958, Berge obteve uma versão generalizada ``para grafos
arbitrários'', tendo como base o teorema de Tutte.

Seja \G\ um grafo \((A,B)\)-bipartido. Para \(X\subseteq A\), definimos
\[
\defi{\(\deficiencia_A(X)\)}=|X|-|\adj{X}|.
\]
chamado a \defi{deficiência de \(X\)}. A \defi{deficiência} de \G,
denotada por \(\deficiencia{G}\) é
\[
\deficiencia (G) = \max \{\deficiencia_A(X ): A\subseteq X\}
\]
(note que a deficiência é não-negativa --- basta tomar \(X=\vazio\)).

\begin{teorema}[Ore]
  Se \G\ é um grafo \((A,B)\)-bipartido, então \(\emp \G=|A|-\deficiencia_A(G)\).
\end{teorema}

\begin{prova}
\(\emp (G)\leq\cdots\) segue da definição de \(\deficiencia_A(G)\). Por
outro lado, \(\emp (G)\geq\cdots\) decorre do corolário \ref{cor:emparelhamento |A| -k}.
\end{prova}

Deficiência de um emparelhamento é o número de vértices descobertos,
\(\deficiencia G=|V|-2\emp G\). Note que essa definição geral
``casa'' com nossa definição anterior quando o grafo é bipartido.

\section{Definição em grafos arbitrários}

Seja \M\ um emparelhamento em \G. Definimos o \defi{defeito de \M}
como sendo o número mínimo e vértices não cobertos por \M. Definimos a
\defi{deficiência de \G} como sendo o número de vértices não cobertos
por um emparelhamento máximo, isto é, 
\begin{align*}
  \deficiencia(G) 
  &\igual |V| -2\emp G, \\
  &\igual \min_{\emp M}\bigl\{|V|-2|M|\bigr\}.
\end{align*}


Berge provou o seguinte resultado (\defi{fórmula de Berge})
\begin{equation}\label{eq:formula de Berge}
\deficiencia G = \max\bigl\{c_i(G-S) -|S| : S\subseteq V\bigr\}.
\end{equation}

\begin{teorema}[Fórmula de Tutte-Berge]
  Seja \G\ um grafo. Então
\begin{equation}\label{eq:formula de Tutte-Berge}
\emp G = \min_{S\subseteq V} \Bigg\{
\frac{|V| +|S| -c_i(G -S)}{2}
\Bigg\}.
\end{equation}
\end{teorema}

\begin{prova}
Primeiramente observe que se existe \(S \subseteq V\) tal que
\(c_i(G-S) > |S|\),
pelo menos \(c_i(G-S) - |S|\) vértices devem ficar desemparelhados em
qualquer emparelhamento.
Assim, temos no máximo \(\frac{|V| - c_i(G-S) + |S|}{2}\) arestas em
um emparelhamento.
Logo, \(Emp(G) \leq \min_{S \subseteq V} \left\{ \frac{|V| + |S| -
  c_i(G-S)}{2}\right\}\).

A demonstração da outra desigualdade segue por indução no número de
vértices \(|V(G)|\).

Se \(|V(G)| = 1\) a fórmula é óbvia: \(Emp(G) = 0\) e \(\min_{S
  \subseteq V} \left\{ \frac{|V| + |S| - c_i(G-S)}{2}\right\} = 0\);

Suponha então que \(|V(G)| > 1\) e que o teorema vale para todo grafo
\(G'\) com \(|V(G')| < |V(G)|\).

Temos dois casos:
\((1)\) Se existe um vértice que é coberto por todo emparelhamento
máximo e 
\((2)\) se para todo vértice \(v \in V(G)\) existe um emparelhamento
máximo de \G\ 
que não o cobre.

No primeiro caso, seja \vv\ um vértice  coberto por todo
emparelhamento máximo de~\G\ e
seja~\M\ um emparelhamento máximo de~\G.
Considere o grafo \(G' \deq G - v\) obtido de~\G\ pela remoção do vértice
\vv\ e arestas de nele incidem.
Seja \(e\) a aresta de \M\ que cobre \vv\ e tome o emparelhamento \(M'
= M-e\) de \(G'\).
Observe que se existe emparelhamento de~\(G'\) de tamanho \(|M'| + 1 =
|M| = Emp(G)\),
então existe emparelhamento máximo para~\G\ que não emparelha~\vv.
Logo \(Emp(G') = Emp(G) -1\).
Pela hipótese de indução, existe \(S' \subseteq V(G')\) tal que
\(\frac{|V'| + |S'| - c_i(G'-S')}{2} = |M'|\).
Considere o conjunto \(S\deq S'\cup \{\vv\} \subseteq V(G)\) e observe que,
uma vez que \(G' = G -v\), temos \(G' - S' = G - v - S' = G - S\).
Logo, temos \(c_i(G'-S') = c_i(G-S)\) e, portanto,
\begin{align*}
|M|
&\igual
|M'| + 1\\
&\igual
\frac{|V'| + |S'| - c_i(G'-S')}{2} + 1\\
&\igual
\frac{(|V'| +1) + (|S'|+1) - c_i(G'-S')}{2}\\
&\igual
\frac{|V| + |S| - c_i(G-S)}{2}\\
&\gigual
\min_{S \subseteq V} \left\{ \frac{|V| + |S| - c_i(G-S)}{2}\right\}\\
\end{align*}

Suponha então que para todo vértice de \G\ existe um emparelhamento
máximo que não o cobre.
Vamos provar que exatamente um vértice fica descoberto em cada
emparelhamento máximo.
Suponha que para todo emparelhamento máximo, existam dois vértices
que não são cobertos.
Tome então um emparelhamento máximo \M\ tal que a distância entre
dois vértices descobertos \vu\ e \vv\ seja mínima.
A distância entre \vu\ e \vv\ não pode ser \(1\), caso contrário
podemos adicionar a aresta \vu\vv\ a \M\ e obter um emparelhamento
maior.
Além disso, todo vértice intermediário num caminho mínimo de \vu\ para
\vv\ deve ser coberto por \M, 
senão existiria um par de vértices não cobertos distando menos que
\(d(\vu, \vv)\).
Tome \(s\) um tal vértice e tome \(N\) um emparelhamento máximo de
\G\ que não cobre \(s\) e tal que \(M\cap N\) seja o maior possível.
Note, em particular, que \vu\ e \vv\ são cobertos por \N\ (e \emph{não} são cobertos por \M). Ora, como a cardinalidade de \M\ e \N\ é a mesma, existe um vértice \(x\neq s\) que é coberto por \M, mas não é coberto por \(N\).
Seja \(y\in V(G)\) o vértice emparelhado com \(x\) em \M.
Se \(y\) não for coberto por \(N\) podemos adicionar a aresta \(xy\) a
\(N\), entrando em contradição com a maximalidade de \(N\).
Se \(y\) for coberto por \(N\), podemos retirar de \(N\) a aresta que
o cobre e adicionar \(xy\) em seu lugar, obtendo um emparelhamento
\(N'\) com uma interseção maior com \M.
\end{prova}


